const puppeteer = require('puppeteer');

var dev = process.argv[2];
console.log('Screenshoting ' + dev);
var fs = require('fs');
var toTest = JSON.parse(fs.readFileSync(dev + '.json', 'utf8'));
if(process.argv[4]) {
  var mod = JSON.parse(fs.readFileSync(process.argv[4] + '.json', 'utf8'));
  let newTest = {};
  mod['only'].forEach(name => {
    console.log('Using ' + name + ' from ' + dev);
    newTest[name] = {};
    newTest[name]['url'] = toTest[name]['url'];
    newTest[name]['description'] = toTest[name]['description'];
  });
  toTest = newTest;
  dev = process.argv[4];
  console.log('Now screenshoting ' + dev);
}

(async () => {
  var fs = require('fs');
  var web = '# ' + process.argv[3] + '\n';
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.setViewport({
    width: 1920,
    height: 1080,
    deviceScaleFactor: 1,
  });
  await page.goto('http://192.168.1.1/reforis/');
  var i = 101;

  // Handle login page
  await page.screenshot({path: '100_' + dev + '_login.png'});
  await page.type('#inputPassword', 'turris');
  await page.click('button');
  await page.waitFor('.spinner-border', {visible: true});

  // Handle the rest
  let screens = toTest[dev];
  for(var name in toTest) {
    let fileName = String(i) + '_' + dev + '_' + name + '.png';
    let url = 'http://192.168.1.1' + toTest[name]['url'];
    web += '\n## ' + toTest[name]['description'] + '\n';
    web += '\n![' + toTest[name]['description'] + '](' + fileName + ')\n';
    console.log(url + ' -> ' + fileName);
    await page.goto(url);

    // try to wait for spinner to appear (not always visible)
    let wait = page.waitFor('.spinner-border', {visible: true, timeout: 4000});
    // ignore errors when there is no spinner
    Promise.resolve(wait).then(function() {}, function() {});

    // wait for spinner to dissappear and take screenshot
    await page.waitFor('.spinner-border', {hidden: true, timeout: 60000});
    await page.screenshot({path: fileName, fullPage: true});
    i++;
  }

  await browser.close();
  fs.writeFile(dev + '.md', web, (err) => { if (err) throw err; });
})();
